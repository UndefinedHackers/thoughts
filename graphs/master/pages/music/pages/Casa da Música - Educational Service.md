- ## Why is it relevant?
  
  Education is always relevant, it’s even a human right. But it’s interesting to understand why a concert hall would put the effort into maintaining an educational service.
  
  First and foremost, Casa da Música is a cultural space and an icon, not only of Porto but of Portugal. That being said, let’s kickstart with some ideas for the why:
  
  * There’s real proximity with music;
  * It’s easy to find and widely known so, attracts a lot of people, therefore a good place to offer education;
  * It contacts with some of the best musicians worldwide, therefore it can promote an awesome network of connections;
  * It’s external to the formality and frameworks of schools where teachers are forced to follow a programme, which makes often teaching less natural to the teacher.
- **"a revolução cultural não é eu poder ir tocar a todos os sítios, é ir aos sítios e ouvir a música que é feita lá". - Zeca Afonso**
  id:: 623153cc-d3eb-4dc2-8b4a-1ce4490a5292
- This sentence transmits the idea that "the music depends on the spaces" and that "Music is something that is done, and not something that is achieved". Which are some concepts that we’ve mentioned in the ((62310b26-4640-4587-bf5c-2435cb6b1c2a)).
- ## How is the Educational Service organised?
- ### Performances
  * First Concerts: For preschoolers;
  * Our Concerts: For schools;
  * Pre-concert Lectures;
  * Commented Concerts.
- ### Workshops
  * First Workshops: For preschoolers;
  * Our Workshops: For schools;
  * Special Weeks: Exploring different, unusual instruments;
  * Major Friday;
  * Workshop of the Day.
- ### Training Sessions
  * Training at Casa da Música: Technical courses;
  * Free Courses: Continual education such as "How to listen to contemporary music"
- ### Out of this World
  Miscellaneous initiatives - Projects, Hotspots, and Roaming - that don't fit the other major categories, current examples are:
  * https://orelhudo.casadamusica.com 
  * Open Rehearsals
  * Children's choir
- ## But none of these initiatives is in "the more known form of education"?
  Rui Canário, which was a professor at the University of Lisbon who studied education and training politics, in the []number 67 of NOESIS magazine](https://www.dge.mec.pt/sites/default/files/CDIE/RNoesis/noesis_miolo67.pdf), opinion article “Aprender sem ser ensinado” (page 21 of the pdf), wrote:
  > Se aprender é algo de intrínseco ao ser humano, também é verdade que o ensino não é uma condição necessária nem suficiente para que se verifique uma aprendizagem. Aprendem-se coisas que não são ensinadas e ensinam-se coisas que ninguém aprende. Felizmente, a maior parte das situações de aprendizagem que vivemos não são formalizadas, no sentido de obedecerem aos requisitos do modelo escolar, nem sequer deliberadas (não há consciência de que o principal objectivo seja aprender algo). A aprendizagem surge, então, como um co-produto da acção.
  
  (the plain text can be found at http://direito.webview.pt/troca.php?no=37)
  
  This idea is that Education is a process of facilitating the resources and means so that one can learn without being taught. “Let me show you what I can do, now you want to learn it, right? :)”
- ### When is education justified?
  We educate for what is complex and non-natural, like eating with cutlery.
  
  Rewinding to ["a revolução cultural não é eu poder ir tocar a todos os sítios, é ir aos sítios e ouvir a música que é feita lá"](((623153cc-d3eb-4dc2-8b4a-1ce4490a5292))), we immediately understand that to get comfortable is an easy trap. We need to explore the unknown. An idea already expressed by Fernando Pessoa with:
  > ((bc9e446b-4f00-464f-a54d-a13cbbc722c6)).
- Think of Europeans trying sushi for the first time. If one is at home and just ordered it, he might not even try to use the chopsticks and simply resort to its loyal fork. But if in a sushi restaurant, and with someone showing him how to use them, he will try and get used to it.
- The human being is lazy by default, which calls for the need of calling for action in exploring the unknown. In the end, that effort and bravery lead to a more fulfilling and rewarding experience. In the particular case of music, among other things, it expands our capability of expression (think Jazz).
- ## Important Takeaways from Jorge Prendas
  
  Jorge Prendas collaborates with this service from Casa da Música since 2007 and started coordinating it in September of 2010. He also helped to create the Cappella quintet Vozes da Radio in 1991.
  * Aprendizagem por transmissão VS **O que levas daqui**: Never ask "what have you learned". Socrates said, "Education is the kindling of a flame, not the filling of a vessel."
  * Every concert of this service aims to answer an underlying motto question.
  * A key principle of every community project is never to be in a possible light of being looked at condescendingly. This meets the idea of [the quote by Zeca Afonso referred to earlier](((623153cc-d3eb-4dc2-8b4a-1ce4490a5292))). The adopted approach to achieve this goal has been co-creation.
  * A very succinct cultural aspect that expresses what should be the aim of any community project is: "Give, Get, Share: Here and Now". This revolves around ideas of the project (Give, Get, Share), context (Here) and Time (Now).
  * The stage is, by its fundamental usage, a plural space that promotes inclusion. This is a characteristic that, through the educative service, Casa da Musica strives to transfer to society.
  * The Educational Service does its best to involve the accompanying team of the audience in its educational actions. That way there can be a follow-up and a more holistic understanding of them before, during, and after the project.