- ## The Project
  
  Casa da Música is a concert hall in Porto, Portugal. It was designed by architect Rem Koolhaas (Pritzker Prize winner).
  
  The project was announced in 1 September 1998. It was intended to be ready by 2001 (as part of the Porto's 2001 tenure as the European Capital of Culture). Unfortunately, construction occurred in the next four years over schedule because of its unusual configuration (such as very pitched walls that took more time than predicted to solidify).
- ## Architecture
  
  The building is shaped as a nine-floor-high asymmetrical polyhedron covered in plaques of white cement, cut by large undulated or plane glass windows.
  
  For photos check the following page on your desktop browser: https://www.casadamusica.com/pt/a-casa-da-musica/espacos/?lang=pt
- ## Groups
  Casa da Música is organised in different groups (named residentes):
  * Orquestra Sinfónica do Porto CdM (originally Orquestra Sinfónica do Conservatório de Música do Porto (1947))
  * Remix Ensemble CdM (formed in 2000 to perform contemporary music)
  * Orquestra Barroca CdM (formed in 2006 to perform Baroque music from a historically-informed perspective)
  * Coro CdM (formed in 2009)
  * Serviço Educativo CdM
  * Coro Infantil CdM (formed in 2007)