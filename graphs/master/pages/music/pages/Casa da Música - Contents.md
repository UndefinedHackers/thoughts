- ![image.png](../../../assets/image_1647446311017_0.png)
- ## Table of Contents
  * [[Casa da Música - Music and Society]]
  * [[Casa da Música - Educational Service]]
- ## Journal
  [[Casa da Música - 2022_03_18 - Concerto Prometeu]]
-
- ## Bibliography
  id:: 62310b26-4640-4587-bf5c-2435cb6b1c2a
  * Byrne, D. How music works. San Francisco Calif: McSweeney's, 2012.
  This book explains concepts such as "the music depends on the spaces", reflecting about how before sound recording was a thing, one depended completely on the space in order to listen to music.
  * Small, C. Musicking : the meanings of performing and listening. Hanover: University Press of New England, 1998.
  This book explains, among other things, that "Music is something that is done, and not something that is achieved". And that's the spirit of traditional music, it is transferred from generation to generation. Think of Virtuoso acts vs pop music - Who reaches more audience and which is more technically challenging.
  * Wold, Milo Arlington and Edmund Cykler; An Outline History of Music, 7th ed. Chicago: University of Chicago Press, 2007
  * Grout, Donald Jay; A History of Western Music, 5th edition. New York: Norton, 1996
  * Blacking, John; How Musical is Man?, Seattle: University of Washington Press, 1973