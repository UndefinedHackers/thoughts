title:: Casa da Música - 2022_03_18 - Concerto Prometeu
type:: Concert

- The Educational Service of Casa da Música organised Prometeu in Sala Suggia performed by the Orquestra Sinfónica do Porto.
- Prometeu is part of the cycle "Música e Mito".
- Sala SUGGIA
- ## Description
- ORQUESTRA SINFÓNICA DO PORTO CASA DA MÚSICA
- MICHAEL SANDERLING direcção musical
-
- LUDWIG VAN BEETHOVEN Abertura de As Criaturas de Prometeu
- CAMILLE SAINT-SAËNS Dança Macabra, op. 40
- FRANZ LISZT Prometeu
- —
- RICHARD WAGNER Suite de O Crepúsculo dos Deuses
-
- Escrito numa fase crucial da carreira de Beethoven, As Criaturas de Prometeu é uma obra baseada no mito grego de Prometeu — o rebelde que roubou o fogo a Zeus para o entregar à humanidade e a libertar da ignorância, abrindo-lhe as portas da ciência e da arte. A narrativa era cara ao compositor, conhecido apoiante dos ideais iluministas, que fez dela o seu único bailado. A mesma lenda serviu a Liszt para um dos seus poemas sinfónicos, explorando magistralmente as emoções associadas à coragem do protagonista e à crueza do seu destino. Já o Anel de Wagner, baseado na mitologia do norte da Europa, retrata outro tipo de sede: a do poder desmesurado, gerando toda a magnífica saga que termina com a ópera O Crepúsculo dos Deuses. O exemplo de Liszt inspirou o francês Saint-Saëns, que se dedicou também aos poemas sinfónicos. Estabelecendo uma ligação com a primeira obra deste programa, a Dança Macabra tem origem no poema “Égalité, fraternité…” de Henri Cazalis, recuperando a lenda medieval da Dança da Morte, em que todos são iguais e igualmente conduzidos, dançando, para a sepultura.
-
-
- “Com um domínio excepcional de tempos e dinâmicas, Sanderling evita o uso de peso excessivo e texturas densas e abrasivas, salvo quando estritamente necessário.”
- MUSIC WEB INERNATIONAL
- ## Understanding how Prometeu fits this cycle
  
  Prometeu was a descendent of the first titans. His name means "the provident", the one that is well prepared for the future, as can foresight it.
- Prometeu's twin brother was "Epimeteu", the one that sees after.
- Because Prometeu knew that Zeus would win the battle against the titans, he decided to join his side. But never forgave this being the destroyer of his race.
- One day, Prometeu deceived Zeus.
- Because Prometeu liked the humans so much, Zeus took the fire from the humans and forced Prometeu to live among them.
- Without fire, the humans had cold and hunger. Because of that, Prometeu asked Atena, his protector, to help him enter the Olympus.
- Prometeu had taught the humans about the arts of architecture, astronomy, and heal, which he learned from Atenas. She once again helped him.
- When Prometeu was back in Olympus, he lighted a torch in the fire vehicle of the Sun, and brought it to earth hidden in a tree, and gave to the humans.
- Prometeu was locked to a mountain, where a crow would eat his liver, only so it would be reconstructed by night. Making it an eternal pain.
- This will of helping humanity that Prometeu reveals comes from the sign of Aquarius.
- No longer be able to use fire, means loosing reason, knowledge, and liberty.
- Prometeu risked his life to give humans a creative fire, but, from the point of views of gods, he makes a sin.
- It is often that, when humans do some effort in the direction of elevation, they also feel like they are making a sin; that they are entering the realm of gods.
- In the book of genesis, God forbids the man of eating the fruit of the knowledge tree. It is apparent a relation between the figure of Zeus, and jealous of the humans' achievements. Such as the God from the bible, who also doesn't want the man to access the knowledge of good and evil.
- Aquarium is a sign of science, intuition and the ideals of freedom, equality.
-