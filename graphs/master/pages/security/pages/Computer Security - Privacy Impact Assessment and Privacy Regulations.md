- ## Privacy Regulations
  
  Recent regulations impose restrictions on the use of Personally Identifiable Information (PII)
  
  General Data Protection Regulation (GDPR) in EU: Applies to all personal data of EU citizens (Independently of organisations collecting the data being located within the EU), and all other people whose data is stored within the EU, even if not EU citizens.
  
  Rights of data subjects (summary):
  * The right to be informed where personal data are collected
  * The right to rectification
  * The right to erasure (right to be forgotten)
  * The right to restrict processing
  * The right to object
  * Rights in relation to automated decision making and profiling
- ## Anonymity in Data Publishing
  
  Entities wish to release data collections either publicly or to third parties for data analysis without disclosing the identity of users.
  
  It's important to take care with **Correlation** and **traceback**: the integration of multiple data sources and information flows to determine the source of a particular data stream or piece of information.
  
  ![image.png](../assets/image_1647449108540_0.png)