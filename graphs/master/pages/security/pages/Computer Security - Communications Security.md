- ## Threats and Attacks
- ### Eavesdropping
  The interception of information intended for someone else during its transmission over a communication channel.
  
  ![image.png](../assets/image_1647448587110_0.png)
- ### Alteration
  Unauthorised modification of information.
  Example: the man-in-the-middle attack, where a network stream is intercepted, modified, and re-transmitted.
  
  ![image.png](../assets/image_1647448677153_0.png)
- ### Denial-of-service
  The interruption or degradation of a data service or information access.
  Example: email _spam_, to the degree that it is meant to simply fill up a mail queue and slow down an email server.
  
  ![image.png](../assets/image_1647448739516_0.png)
- ### Masquerading
  The fabrication of information that is purported to be from someone who is not actually the author.
  
  ![image.png](../assets/image_1647448792787_0.png)
- ### Repudiation
  The denial of a commitment or data receipt.
  This involves an attempt to back out of a contract or a protocol that requires the different parties to provide receipts acknowledging that data has been received.
- ### Replay Attack
  When the attacker eavesdropped and repeats the request, works for simply authentication over insecure channels. Can be prevented, for example, by using a nonce.