- ## Network and Internet Security
  
  This field consists of measures to deter, prevent, detect, and correct security violations that involve the transmission of information.
- ## Computer Security
  
  The NIST (National Institute of Standards and Technology (USA)) Computer Security Handbookdefines the term computer security as:
  > “the protection afforded to an _automated information system_ in order to attain the applicable objectives of preserving the _integrity, availability and confidentiality_ of information system resources” (includes hardware, software, firmware, information/data, and telecommunications)
- ### Computer Security Objectives
  
  **Confidentiality**:
  * Data confidentiality: Assures that private or confidential information is not made available or disclosed to unauthorised individuals
  * Privacy: Assures that individuals control or influence what information related to them may be collected and stored and by whom and to whom that information may be disclosed
  **Integrity**:
  * Data integrity: Assures that  information and programs are changed only in a specified and authorised manner
  * System integrity: Assures that a system performs its intended function in an unimpaired manner, free from deliberate or inadvertent unauthorised manipulation of the system
  **Availability**: Assures that systems work promptly and service is not denied to authorised users
- ## Essential Network and Computer Security Requirements
  ![image.png](../assets/image_1647447811223_0.png)
- ## Confidentiality
  
  Confidentiality is the avoidance of the unauthorised disclosure of information.
  It involves the _protection of data_, providing access for those who are allowed to see it while disallowing others from learning anything about its content.
  
  Tools for Confidentiality:
  * **Encryption**: the transformation of information using a secret, called an encryption key, so that the transformed information can only be read using another secret, called the decryption key (which may, in some cases, be the same as the encryption key).
  * **Access control**: rules and policies that limit access to confidential information to those people and/or systems with a "need to know" (this need to know may be determined by identity, such as a person’s name or a computer’s serial number, or by a role that a person has, such as being a manager or a computer security specialist).
  * **Authentication**: the determination of the identity or role that someone has.
  * **Authorization**: the determination if a person or system is allowed access to resources, based on an access control policy: Such authorizations should prevent an attacker from tricking the system into letting him have access to protected resources.
- #### Authentication
  This determination can be done in a number of different ways, but it is usually based on a combination of:
  * something the person has (like a smart card or a radio key for storing secret keys),
  * something the person knows (like a password),
  * something the person is (like a human with a fingerprint).
- ## Integrity
  The property that information has not be altered in an unauthorised way.
  Tools:
  * **Hash functions**: One way functions.
  * **Backups**: the periodic archiving of data.
  * **Checksums**: the computation of a function that maps the contents of a file to a numerical value. A checksum function depends on the entire contents of a file and is designed in a way that even a small change to the input file (such as flipping a single bit) is highly likely to result in a different output value.
  * **Data correcting codes**: methods for storing data in such a way that small changes can be easily detected and automatically corrected.
- ## Availability
  The property that information/systems are accessible and modifiable in a timely fashion by those authorised to do so.
  Tools:
  * Physical protections: infrastructure meant to keep information available even in the event of physical challenges.
  * Computational redundancies: computers and storage devices that serve as fallbacks in the case of failures.
- ## Authenticity
  The ability to determine that statements, policies, and permissions issued by persons or systems are genuine.
  
  The primary tool are **digital signatures**. These are cryptographic computations that allow a person or system to commit to the authenticity of their documents in a unique way that achieves **nonrepudiation**, which is the property that authentic statements issued by some person or system cannot be denied.