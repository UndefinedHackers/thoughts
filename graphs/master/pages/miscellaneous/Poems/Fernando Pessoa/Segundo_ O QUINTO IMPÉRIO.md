public:: true
title:: Segundo: O QUINTO IMPÉRIO
author:: Fernando Pessoa
book:: Mensagem
source:: http://arquivopessoa.net/textos/96

- Triste de quem vive em casa,
  id:: bc9e446b-4f00-464f-a54d-a13cbbc722c6
  Contente com o seu lar,
  Sem que um sonho, no erguer de asa,
  Faça até mais rubra a brasa
  Da lareira a abandonar!
- Triste de quem é feliz!
  Vive porque a vida dura.
  Nada na alma lhe diz
  Mais que a lição da raiz —
  Ter por vida a sepultura.
- Eras sobre eras se somem
  No tempo que em eras vem.
  Ser descontente é ser homem.
  Que as forças cegas se domem
  Pela visão que a alma tem!
- E assim, passados os quatro
  Tempos do ser que sonhou,
  A terra será teatro
  Do dia claro, que no atro
  Da erma noite começou.
- Grécia, Roma, Cristandade,
  Europa — os quatro se vão
  Para onde vai toda idade.
  Quem vem viver a verdade
  Que morreu D. Sebastião?